import React, {Component} from 'react';
import axios from 'axios'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css'

class App extends React.Component {
  constructor(props) {
    super(props);
      this.state = {
        users: [],
        isLoading: true,
        limit:10,
        errors: null
      };
      this.loadMore = this.loadMore.bind(this);
  }
  loadMore() {
    this.setState((prev) => {
      return {limit: prev.limit + 10};
    });
  }

  componentDidMount() { 
    axios
    .get("https://randomuser.me/api/?results=100")
    .then(response =>
      response.data.results.map(user => ({
        name: `${user.name.first} ${user.name.last}`,
        age: `${user.dob.age}`,
        location: `${user.location.city}, ${user.location.state} ${user.location.postcode}`,
        image: `${user.picture.thumbnail}`
      }))
    )
    .then(users => {
      this.setState({
        users,
        isLoading: false
      });
    })
    .catch(error => this.setState({ error, isLoading: false }));
  }
  sortCity() {

  }
  render() {
    return (
      <React.Fragment>
        <div class="container">
            <div class="row">
                <div class="col text-center">
                  <button onClick={this.sortCity} class="btn btn-success margin">SORT</button>
                </div>
            </div>
            <div className="feed">            
                  {this.state.users.slice(0, this.state.limit).map((user) => {
                      return (
                        <div className="card margin">
                              <div className="card-body">
                                <div class="row">
                                  <div class="col-md-3">
                                      <img src={user.image} alt={user.name}  />
                                  </div>      
                                  <div class="col-md-3">             
                                    <h5 className="card-title">{user.name} {user.age}</h5>
                                    <h6 className="card-subtitle mb-2 text-muted">{user.location} </h6>
                                  </div>
                                </div>
                              </div>
                            </div>
                      );
                    })}                  
                  {this.state.limit < this.state.users.length &&
                    <button onClick={this.loadMore} type="button" className="btn btn-primary btn-lg btn-block margin">Load more</button>
                  }
            </div>    
        </div>
      </React.Fragment>
    );
  }
}


export default App;
